public inherited sharing class SecurityController {


    public static SObject doUpsert( SObject sObj) {
        if ( !sObj.getSObjectType().getDescribe().isUpdateable() || !sObj.getSObjectType().getDescribe().isCreateable() ) {
            throw new VT_Exception('Insufficient create/update access. ( Object: '+sObj.getSObjectType() +' )' );
        }
        SObjectAccessDecision securityDecision = getAccessibleDataSObjectAccessDecision(new List<SObject>{sObj},'upsert');
        upsert securityDecision.getRecords();
        return securityDecision.getRecords()[0];

    }

    public static SObject doInsert( SObject sObj ) {
        if (!sObj.getSObjectType().getDescribe().isCreateable() ) {
            throw new VT_Exception('Insufficient create access. ( Object: '+sObj.getSObjectType() +' )' );
        }
        SObjectAccessDecision securityDecision = getAccessibleDataSObjectAccessDecision(new List<SObject>{sObj},'insert');
        insert securityDecision.getRecords();
        return securityDecision.getRecords()[0];
    }


    public static SObject doUpdate( SObject sObj) {
        if ( !sObj.getSObjectType().getDescribe().isUpdateable() ) {
            throw new VT_Exception('Insufficient update access. ( Object: '+sObj.getSObjectType() +' )' );
        }
        SObjectAccessDecision securityDecision = getAccessibleDataSObjectAccessDecision(new List<SObject>{sObj},'update');
        update securityDecision.getRecords();
        return securityDecision.getRecords()[0];
    }

    public static SObjectAccessDecision getAccessibleDataSObjectAccessDecision( List<sObject> sObjList, String dmlAction){
        dmlAction = dmlAction.toLowerCase();
        SObjectAccessDecision securityDecision;
        
        switch on dmlAction {
            when 'query'{
                securityDecision = Security.stripInaccessible(
                    AccessType.READABLE, sObjList);
            }
            when 'insert'{
                securityDecision = Security.stripInaccessible(
                    AccessType.CREATABLE, sObjList);
            }
            when 'update'{
                securityDecision = Security.stripInaccessible(
                    AccessType.UPDATABLE, sObjList);
            }
            when 'upsert'{
                securityDecision = Security.stripInaccessible(
                    AccessType.UPSERTABLE, sObjList);
            }           
        }
        return securityDecision;
    }
}
